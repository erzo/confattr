confattr.formatters module
==========================

.. automodule:: confattr.formatters
   :members:
   :undoc-members:
   :show-inheritance:
