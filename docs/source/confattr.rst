confattr package
================

Submodules
----------

.. toctree::
   :maxdepth: 4

   confattr.config
   confattr.configfile
   confattr.subprocess_pipe
   confattr.types

Module contents
---------------

.. automodule:: confattr
   :members:
   :undoc-members:
   :show-inheritance:
