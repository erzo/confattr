Config Attributes

A python library to read and write config files
with a syntax inspired by vimrc and ranger config.

Documentation: https://erzo.gitlab.io/confattr/latest
